package com.util;

public class StringUtils {
	
	public static String stringRev(String str) {
		StringBuilder sb = new StringBuilder(str);

		return sb.reverse().toString();
	} 

}
