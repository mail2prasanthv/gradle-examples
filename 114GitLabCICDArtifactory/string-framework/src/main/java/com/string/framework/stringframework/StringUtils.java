package com.string.framework.stringframework;

import org.springframework.stereotype.Service;

@Service
public class StringUtils {
	
	public  String reverse(String val) {
		StringBuilder sb = new StringBuilder(val); 
		return sb.reverse().toString();
	}

}
