package com.string.framework.stringframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringFrameworkApplication.class, args);
	}

}
